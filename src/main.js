import Vue from 'vue'
import vuetify from '@/plugins/vuetify'
import 'bulma/css/bulma.css'

import App from './App'
import router from './router'

new Vue({
    vuetify,
}).$mount('#app')

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    components: { App },
    template: '<App/>'
})